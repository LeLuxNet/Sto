package sto

import (
	"errors"
	"io"
)

var (
	ErrNotFound = errors.New("sto: not found")
)

type Bucket interface {
	Object(id Id) (io.ReadSeekCloser, error)
	HasObject(id Id) (bool, error)
	SetObject(r io.Reader) (Id, error)
}
