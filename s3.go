package sto

import (
	"bytes"
	"encoding/hex"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"time"

	"github.com/aws/aws-sdk-go/aws/credentials"
	v4 "github.com/aws/aws-sdk-go/aws/signer/v4"
	xhttp "lelux.net/x/net/http"
)

type S3 struct {
	Endpoint string
	Region   string
	Bucket   string

	HTTPClient *http.Client

	AccessKey string
	SecretKey string
}

func (b S3) httpClient() *http.Client {
	if b.HTTPClient == nil {
		return http.DefaultClient
	}
	return b.HTTPClient
}

func (b S3) req(method string, path string, body io.ReadSeeker, sha256 []byte) (*http.Response, error) {
	req, err := http.NewRequest(method, fmt.Sprintf("%s/%s/%s", b.Endpoint, url.PathEscape(b.Bucket), path), body)
	if err != nil {
		return nil, err
	}

	if sha256 != nil {
		req.Header.Set("X-Amz-Content-Sha256", hex.EncodeToString(sha256))
	}

	signer := v4.NewSigner(credentials.NewStaticCredentials(b.AccessKey, b.SecretKey, ""))
	_, err = signer.Sign(req, body, "", b.Region, time.Now())
	if err != nil {
		return nil, err
	}

	res, err := b.httpClient().Do(req)
	if err != nil {
		return nil, err
	}

	return res, nil
}

func (b S3) Object(id Id) (io.ReadSeekCloser, error) {
	res, err := b.req(http.MethodGet, id.String(), nil, nil)
	if err != nil {
		return nil, err
	}

	switch res.StatusCode {
	case http.StatusOK:
		b, err := io.ReadAll(res.Body)
		if err != nil {
			return nil, err
		}
		return resCloser{bytes.NewReader(b), res.Body}, nil
	case http.StatusNotFound:
		res.Body.Close()
		return nil, ErrNotFound
	default:
		res.Body.Close()
		return nil, xhttp.UnexpectedStatus(res.Status)
	}
}

type resCloser struct {
	io.ReadSeeker
	io.Closer
}

func (b S3) HasObject(id Id) (bool, error) {
	res, err := b.req(http.MethodHead, id.String(), nil, nil)
	if err != nil {
		return false, err
	}
	defer res.Body.Close()

	switch res.StatusCode {
	case http.StatusOK:
		return true, nil
	case http.StatusNotFound:
		return false, nil
	default:
		return false, xhttp.UnexpectedStatus(res.Status)
	}
}

func (b S3) SetObject(r io.Reader) (Id, error) {
	data, err := io.ReadAll(r)
	if err != nil {
		return nil, err
	}

	id, err := GetFileId(bytes.NewReader(data))
	if err != nil {
		return nil, err
	}

	// check whether object already exists
	has, err := b.HasObject(id)
	if err != nil {
		return nil, err
	}
	if has {
		return id, nil
	}

	idStr := id.String()

	// upload object
	res, err := b.req(http.MethodPut, idStr, bytes.NewReader(data), id)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()

	if res.StatusCode != http.StatusOK {
		return nil, xhttp.UnexpectedStatus(res.Status)
	}

	return id, nil
}
