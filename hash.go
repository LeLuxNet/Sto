package sto

import (
	"crypto/sha256"
	"io"
)

var fileIdSize = sha256.Size

func GetFileId(r io.Reader) (Id, error) {
	h := sha256.New()

	_, err := io.Copy(h, r)
	if err != nil {
		return nil, err
	}

	return h.Sum(nil), nil
}
