package sto

import (
	"errors"
	"fmt"
	"io"
)

type Cache struct {
	Bucket
	Cache Bucket
}

func (b Cache) Object(id Id) (io.ReadSeekCloser, error) {
	r, err := b.Cache.Object(id)
	if !errors.Is(err, ErrNotFound) {
		return r, err
	}

	r, err = b.Bucket.Object(id)
	if err != nil {
		return nil, err
	}

	id2, err := b.Cache.SetObject(r)
	if err != nil {
		return nil, err
	}

	if string(id) != string(id2) {
		return nil, fmt.Errorf("sto: cache ids don't match")
	}

	return b.Cache.Object(id)
}
