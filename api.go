package sto

import (
	"errors"
	"fmt"
	"io"
	"log"
	"net/http"
	"time"
)

type API struct {
	Bucket Bucket
}

func (a API) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case http.MethodHead, http.MethodGet:
		a.handleGet(w, r)
	case http.MethodPost:
		a.handlePost(w, r)
	default:
		w.WriteHeader(http.StatusMethodNotAllowed)
	}
}

func (a API) handleGet(w http.ResponseWriter, r *http.Request) {
	if len(r.URL.Path) <= 1 {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	id, err := ParseFileId(r.URL.Path[1:])
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	obj, err := a.Bucket.Object(id)
	if err != nil {
		if errors.Is(err, ErrNotFound) {
			w.WriteHeader(http.StatusNotFound)
		} else {
			log.Println(err)
			w.WriteHeader(http.StatusInternalServerError)
		}
		return
	}
	defer obj.Close()

	idStr := id.String()

	w.Header().Set("X-Content-Type-Options", "nosniff")

	w.Header().Set("ETag", `"`+idStr+`"`)

	http.ServeContent(w, r, "", time.Unix(0, 0), obj)
}

func (a API) handlePost(w http.ResponseWriter, r *http.Request) {
	q := r.URL.Query()
	meta := Meta{
		Name: q.Get("name"),
		Type: q.Get("type"),
	}

	var read io.Reader

	f, fHeader, err := r.FormFile("file")
	switch {
	case errors.Is(err, http.ErrNotMultipart):
		read = r.Body

		if meta.Type == "" {
			meta.Type = r.Header.Get("Content-Type")
		}
	case errors.Is(err, http.ErrMissingFile):
		http.Error(w, "missing 'file' in http form-data", http.StatusBadRequest)
		return
	case err == nil:
		read = f
		defer f.Close()

		if meta.Name == "" {
			meta.Name = fHeader.Filename
		}
		if meta.Type == "" {
			meta.Type = fHeader.Header.Get("Content-Type")
		}
	default:
		log.Println(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	fmt.Println(meta)

	id, err := a.Bucket.SetObject(read)
	if err != nil {
		log.Println(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	_, err = w.Write([]byte(id.String()))
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
}
