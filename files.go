package sto

import (
	"encoding/base64"
	"errors"
	"path/filepath"
)

var (
	ErrInvalidId = errors.New("sto: invalid Id")
)

type Id []byte

var idEnc = base64.RawURLEncoding

func ParseFileId(s string) (Id, error) {
	if idEnc.DecodedLen(len(s)) != fileIdSize {
		return nil, ErrInvalidId
	}
	return idEnc.DecodeString(s)
}

func (f Id) String() string {
	return idEnc.EncodeToString(f)
}

const dirSplit = 4

func idPath(id Id) string {
	s := id.String()
	return filepath.Join(s[:dirSplit], s[dirSplit:])
}
