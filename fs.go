package sto

import (
	"errors"
	"io"
	"io/ioutil"
	"os"
	"path/filepath"
)

func TmpFS() (FS, error) {
	tmp, err := ioutil.TempDir("", "sto")
	return FS{tmp}, err
}

type FS struct {
	Path string
}

func (b FS) objectPath(id Id) string {
	return filepath.Join(b.Path, "objects", idPath(id))
}

func (b FS) Object(id Id) (io.ReadSeekCloser, error) {
	f, err := os.Open(b.objectPath(id))

	switch {
	case errors.Is(err, os.ErrNotExist):
		return nil, ErrNotFound
	default:
		return f, err
	}
}

func (b FS) HasObject(id Id) (bool, error) {
	_, err := os.Stat(b.objectPath(id))
	switch {
	case err == nil:
		return true, nil
	case errors.Is(err, os.ErrNotExist):
		return false, nil
	default:
		return false, err
	}
}

func (b FS) SetObject(r io.Reader) (Id, error) {
	tmpDir := filepath.Join(b.Path, "tmp")
	err := os.MkdirAll(tmpDir, 0700)
	if err != nil {
		return nil, err
	}

	f, err := os.CreateTemp(tmpDir, "")
	if err != nil {
		return nil, err
	}
	defer func() {
		if err != nil {
			os.Remove(f.Name())
		}
	}()

	_, err = io.Copy(f, r)
	if err != nil {
		return nil, err
	}

	_, err = f.Seek(0, io.SeekStart)
	if err != nil {
		return nil, err
	}

	id, err := GetFileId(f)
	if err != nil {
		return nil, err
	}

	path := b.objectPath(id)

	err = os.MkdirAll(filepath.Dir(path), 0755)
	if err != nil {
		return nil, err
	}

	err = os.Rename(f.Name(), path)
	if err != nil {
		return nil, err
	}

	return id, nil
}

func (b FS) RemoveAll() error {
	return os.RemoveAll(b.Path)
}
